# simpleblog

<!-- [![Latest Version on Packagist][ico-version]][link-packagist] -->
<!-- [![Software License][ico-license]](LICENSE.md) -->
<!-- [![Build Status][ico-travis]][link-travis] -->
<!-- [![Coverage Status][ico-scrutinizer]][link-scrutinizer] -->
<!-- [![Quality Score][ico-code-quality]][link-code-quality] -->
<!-- [![Total Downloads][ico-downloads]][link-downloads] -->

A simple file based blog solution for Laravel. Blog pages are `.md` files in a `/blog/` folder in you app folder.

## Install

Via Composer

``` bash
$ composer require revooms/simpleblog
```

Publish the config and view files to your app
``` bash
$ php artisan vendor:publish
```

## Usage
In your controller:

``` php
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Revooms\SimpleBlog\SimpleBlog;

class PageController extends Controller
{
    public function index()
    {
        $blog = new SimpleBlog;
        return view('welcome', ['blog' => $blog]);
    }
}
```

## Testing

``` bash
$ composer test
```
