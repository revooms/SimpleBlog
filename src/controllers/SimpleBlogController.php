<?php

namespace Revooms\SimpleBlog\Controllers;

use Illuminate\Http\Request;
use Revooms\SimpleBlog\BlogPage;
use Revooms\SimpleBlog\SimpleBlog;

class SimpleBlogController
{
    public function index()
    {
        $blog = new SimpleBlog;
        return view('simpleblog::index')->with('blog', $blog);
    }

    public function showPage($slug)
    {
        $blog = new SimpleBlog;
        $page = $blog->getPageBySlug($slug);
        $page->setBodyFromPath();
        return view('simpleblog::show')->with('page', $page);
    }
}
