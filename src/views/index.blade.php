<ol>
@foreach ( $blog->getPages() as $page )
    <li>
        {{ $page->published_at->format('Y/m/d H:i') }}:
        <a href="{{ route('simpleblog.showpage', ['slug' => $page->slug]) }}">{{ $page->title }}</a>
    </li>
@endforeach
</ol>
