<article>
    <h1>{{ $page->title }}</h1>
    <p>
        {{ $page->published_at }}
        by {{ $page->author }}
    </p>
    <section class="content">
        {!! $page->body !!}
    </section>
</article>
