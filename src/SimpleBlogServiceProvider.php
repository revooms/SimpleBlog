<?php

namespace Revooms\SimpleBlog;

use Illuminate\Http\File;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;

class SimpleBlogServiceProvider extends ServiceProvider
{

    // Set this namespace to your package controllers namespace.
    protected $namespace = 'Revooms\SimpleBlog\Controllers';

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot(Router $router)
    {
        $viewsDir = __DIR__.'/views';
        $this->loadViewsFrom($viewsDir, 'simpleblog');
        $this->mapRoutes($this->app->router);
        $this->publishes([
            __DIR__.'/../config/simpleblog.php' => config_path('simpleblog.php'),
            $viewsDir => resource_path('views/vendor/simpleblog'),
        ]);
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make('Revooms\SimpleBlog\controllers\SimpleBlogController');
    }

    protected function mapRoutes(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require(__DIR__.'/routes.php');
        });
    }
}
