<?php

namespace Revooms\SimpleBlog;

use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Model;

class BlogPage extends Model
{
    protected $fillable = ['title', 'published_at', 'author', 'body', 'path', 'slug'];
    protected $dates = ['published_at'];

    public function __construct($path)
    {
        $this->path = $path;
        $this->author = config('simpleblog.author');
        $this->setTitleFromPath();
        $this->setPublishedAtFromPath();
        $this->setSlugFromPath();
    }

    public function setSlugFromPath()
    {
        $split = $this->getSplitBasename();
        $slug = $this->published_at->format('Ymdhi')  . "-" . str_slug($this->title);
        $this->slug = $slug;
    }

    public function setTitleFromPath()
    {
        $split = $this->getSplitBasename();
        $title = str_replace('-', ' ', $split[1]);
        $this->title = title_case($title);
    }

    public function setBodyFromPath()
    {
        $file = file_get_contents($this->path);
        $parser = app('Indal\Markdown\Parser');
        $html = $parser->parse($file); // <h1>Hello</h1>
        $this->body = $html;
    }

    public function setPublishedAtFromPath()
    {
        $split = $this->getSplitBasename();
        // date ($split[0])
        $p = $split[0];
        $y = substr($p, 0, 4);
        $m = substr($p, 4, 2);
        $d = substr($p, 6, 2);
        $h = substr($p, 8, 2);
        $i = substr($p, 10, 2);
        // dd($y, $m, $d, $h, $i);
        $this->published_at = Carbon::create($y, $m, $d, $h, $i, 0);
    }

    private function getSplitBasename()
    {
        $post = basename($this->path, '.md');
        $split = explode('-', $post, 2);
        return $split;
    }
}
