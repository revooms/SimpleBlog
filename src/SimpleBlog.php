<?php

namespace Revooms\SimpleBlog;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\File;

class SimpleBlog
{
    private $path;
    private $pages = [];

    public function getPages()
    {
        return $this->pages;
    }

    public function addPage(BlogPage $page)
    {
        $this->pages[] = $page;
    }


    public function getPageBySlug($slug)
    {
        foreach ($this->pages as $page) {
            if ($page->slug == $slug) {
                return $page;
            }
        }
    }

    /**
     * Create a new Skeleton Instance
     */
    public function __construct($path = null)
    {
        // constructor body
        $this->path = $path ?? config('simpleblog.path');
        // $result = File::makeDirectory($this->path);
        $pagefiles = glob($this->path . '/*.md');
        rsort($pagefiles);
        foreach ($pagefiles as $pagefile) {
            $page = new BlogPage($pagefile);
            $this->addPage($page);
        }
    }
}
